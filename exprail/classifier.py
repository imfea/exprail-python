"""
Sample classifier function
"""


def is_in_class(token_class, token):
    """
    Check that whether the token is in the class or not.
    :param token_class: the name of the token class as a string
    :param token: the considered token
    :return: True, when the token is in the class, else False
    """
    _ = token_class
    _ = token
    raise NotImplementedError('The classifier does not implemented!')
